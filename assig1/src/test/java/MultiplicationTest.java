package test.java;
import main.java.Logic.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MultiplicationTest {

    @Test
    void operation() {
        String poly1 = "x^3-12x^2-42";
        String poly2 = "x^2-2x+1";
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result = new Polinom();
        SimpleResult simpleResult = new SimpleResult();
        p1.initPolinom();
        p2.initPolinom();
        result.initPolinom();
        p1 = p1.stringToPoly(poly1);
        p2 = p2.stringToPoly(poly2);
        Multiplication multiplication = new Multiplication();
        simpleResult = multiplication.operation(p1,p2);
        result = simpleResult.getPolinom();

        String expectedOutput = "x^5-14x^4+25x^3-54x^2+84x-42";

        assertEquals (result.toString(),expectedOutput);
    }
}