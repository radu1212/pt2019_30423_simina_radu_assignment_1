package test.java;
import main.java.Logic.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DivisionTest {

    @Test
    void operation() {
        String poly1 = "x^3-12x^2-42";
        String poly2 = "x^2-2x+1";
        Polinom p1 = new Polinom();
        Polinom p2 = new Polinom();
        Polinom result1 = new Polinom();
        Polinom result2 = new Polinom();
        PairResult pairResult = new PairResult();
        p1.initPolinom();
        p2.initPolinom();
        result1.initPolinom();
        result2.initPolinom();
        p1 = p1.stringToPoly(poly1);
        p2 = p2.stringToPoly(poly2);
        Division division = new Division();
        pairResult = division.operation(p1,p2);
        result1= pairResult.getQuotient();
        result2=pairResult.getRemainder();

        String expectedQuotient = "x-10";
        String expectedRemainder ="-21x-32";

        assertEquals (result1.toString(),expectedQuotient);
        assertEquals(result2.toString(),expectedRemainder);
   }
}