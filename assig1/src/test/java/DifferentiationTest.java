package test.java;
import main.java.Logic.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DifferentiationTest {

    @Test
    void operation() {
        String poly1 = "x^3-12x^2-42";

        Polinom p1 = new Polinom();
        Polinom result = new Polinom();
        SimpleResult simpleResult = new SimpleResult();
        p1.initPolinom();
        result.initPolinom();
        p1 = p1.stringToPoly(poly1);
        Differentiation differentiation = new Differentiation();
        simpleResult = differentiation.operation(p1);
        result = simpleResult.getPolinom();
        String expectedOutput = "3x^2-24x";

        assertEquals (result.toString(),expectedOutput);
    }
}