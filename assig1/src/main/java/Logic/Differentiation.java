package main.java.Logic;

public class Differentiation implements OneOperandOperation{
    @Override
    public SimpleResult operation(Polinom p) {
        Polinom result = new Polinom();
        result.initPolinom();
        for(Monom m:p.getPolinom()){
            Monom temp = new Monom();
            if(m.getDegree()==0){
                temp.setCoef(0);
                temp.setDegree(0);
            }else {
                temp.setCoef(m.getCoef() * m.getDegree());
                temp.setDegree(m.getDegree() - 1);
            }
            result.addMonom(temp);
        }
        SimpleResult simpleResult = new SimpleResult();
        result.orderPolinom();
        simpleResult.setPolinom(result);
        return simpleResult;
    }
}
