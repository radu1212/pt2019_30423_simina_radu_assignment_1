package main.java.Logic;
public interface OneOperandOperation extends Operation{
    public Object operation(Polinom p);

}
