package main.java.Logic;

public class SimpleResult implements Result {
    private Polinom polinom;

    public Polinom getPolinom() {
        return polinom;
    }

    public void setPolinom(Polinom polinom) {
        this.polinom = polinom;
    }
}
