package main.java.Logic;
public class Integration implements OneOperandOperation{

    @Override
    public SimpleResult operation(Polinom p) {
        Polinom result = new Polinom();
        result.initPolinom();
        for(Monom m:p.getPolinom()){
            Monom temp = new Monom();
            float degree = m.getDegree()+1.0f;
            temp.setCoef(m.getCoef()/(degree));
            temp.setDegree(m.getDegree()+1);
            result.addMonom(temp);
        }
        SimpleResult simpleResult = new SimpleResult();
        result.orderPolinom();
        simpleResult.setPolinom(result);
        return simpleResult;
    }
}


