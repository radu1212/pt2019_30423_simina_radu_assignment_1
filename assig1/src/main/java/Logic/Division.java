package main.java.Logic;
public class Division implements TwoOperandsOperation {
    @Override
    public PairResult operation(Polinom p1, Polinom p2) {
        int remainderDegree=p1.getHighestDegree();
        int divisorDegree=p2.getHighestDegree();
        Polinom quotient = new Polinom();
        Polinom remainder = new Polinom();
        quotient.initPolinom();
        remainder.initPolinom();
        remainder = p1;
        Multiplication multiplication = new Multiplication();
        Substraction substraction = new Substraction();
        while(remainderDegree>=divisorDegree && remainderDegree>0){
            p1 = p1.orderPolinom();
            p2 = p2.orderPolinom();
            remainder = remainder.orderPolinom();
            int first = 0;
            Monom temp = new Monom();
            for(Monom m1:remainder.getPolinom()){
                for(Monom m2:p2.getPolinom()) {
                    if (first == 0) {
                        temp.setDegree(m1.getDegree() - m2.getDegree());
                        temp.setCoef( m1.getCoef() /  m2.getCoef());
                        first = 1;
                    }
                }
            }
            quotient.addMonom(temp);
            Polinom aux = new Polinom();
            aux.initPolinom();
            aux.addMonom(temp);
            remainder.setPolinom(substraction.operation(p1,multiplication.operation(aux,p2).getPolinom()).getPolinom().getPolinom());
            remainder.getPolinom().remove(0);
            remainderDegree=remainder.getHighestDegree();
        }
        PairResult pairResult = new PairResult();
        pairResult.setQuotient(quotient);
        Monom m = new Monom();
        if(remainder.getPolinom().isEmpty()) {
            m.setDegree(0);
            m.setCoef(0);
            remainder.getPolinom().add(m);
        }
        pairResult.setRemainder(remainder);
        return pairResult;
    }
}
