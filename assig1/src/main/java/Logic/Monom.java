package main.java.Logic;

public class Monom {

    private float coef;
    private int degree;

    public Monom() { }

    public float getCoef() {
        return coef;
    }

    public void setCoef(float coef) {
        this.coef = coef;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    @Override
    public String toString() {
        int c=(int)coef;
        if(coef != 0 && coef !=1 && coef != -1 && degree != 0 && degree != 1) {
            if(c==coef) return c + "x" + "^" + degree;
            else return coef + "x" + "^" + degree;
        }
        else if(coef != 0 && coef !=1 && degree ==0) {
            if(c==coef) return c + "";
            else return coef + "";
        }
        else if(coef != 0 && coef !=1 && degree ==1){
            if(c==coef) return c + "x";
            else return coef+"x";
        }
        else if(coef == 1 && degree ==1)
            return "x";
        else if(coef == 1 && degree ==0)
            return "1";
        else if(coef == 1 && degree != 0 && degree != 1)
            return "x^"+degree;
        else if(coef == -1 && degree ==1)
            return "-x";
        else if(coef == -1 && degree ==0)
            return "-1";
        else if(coef == -1 && degree != 0 && degree != 1)
            return "-x^"+degree;
        else if(coef == 0 && degree == 0)
            return "0";
        else  return "";
    }
}
