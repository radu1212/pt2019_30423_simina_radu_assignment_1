package main.java.Logic;
public class PairResult implements Result{
    private Polinom quotient;
    private Polinom remainder;

    public Polinom getQuotient() {
        return quotient;
    }

    public void setQuotient(Polinom quotient) {
        this.quotient = quotient;
    }

    public Polinom getRemainder() {
        return remainder;
    }

    public void setRemainder(Polinom remainder) {
        this.remainder = remainder;
    }
}
