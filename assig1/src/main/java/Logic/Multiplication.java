package main.java.Logic;
public class Multiplication implements TwoOperandsOperation {
    @Override
    public SimpleResult operation(Polinom p1, Polinom p2) {
         Polinom result = new Polinom();
         result.initPolinom();
         for(Monom m1:p1.getPolinom()){
             for(Monom m2:p2.getPolinom()){
                 Monom temp = new Monom();
                 temp.setCoef(m1.getCoef() * m2.getCoef());
                 temp.setDegree(m1.getDegree() + m2.getDegree());
                 result.addMonom(temp);
             }
         }
        SimpleResult simpleResult = new SimpleResult();
        result.orderPolinom();
        simpleResult.setPolinom(result);
        return simpleResult;
    }
}
