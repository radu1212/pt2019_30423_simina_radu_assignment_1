package main.java.Logic;
public class Addition implements TwoOperandsOperation {

    @Override
    public SimpleResult operation(Polinom p1, Polinom p2) {
        Polinom result = new Polinom();
        result.initPolinom();
        result = p1;
        for(Monom m:p2.getPolinom()){
            result.addMonom(m);
        }
        SimpleResult simpleResult = new SimpleResult();
        result.orderPolinom();
        simpleResult.setPolinom(result);
        return simpleResult;
    }
}
