package main.java.Logic;
import main.java.Exceptions.*;
import java.util.ArrayList;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Polinom extends Monom {

    private ArrayList<Monom> polinom;

    public void initPolinom(){
        this.polinom = new ArrayList<>();
    }

    public void checkForNullCoef(){
        for(Monom m:polinom){
            if(m.getCoef()==0)
                polinom.remove(m);
        }
    }

    public void addMonom(Monom monom){
        int exist = 0;

        if(polinom.isEmpty()){
            polinom.add(monom);
        }
        else {
            for (Monom temp : polinom) {
                if (monom.getDegree() == temp.getDegree()) {
                    temp.setCoef(temp.getCoef() + monom.getCoef());
                    exist = 1;
                }
            }

            if (exist == 0) {
                polinom.add(monom);
            }
        }

    }

    public Polinom negativePolinom(Polinom p){
        Polinom result = new Polinom();
        result.initPolinom();
        for(Monom m:p.getPolinom()){
            m.setCoef(m.getCoef() * (-1));
            result.addMonom(m);
        }
        return result;
    }

    public Polinom orderPolinom(){
        polinom.sort(Comparator.comparingInt(Monom::getDegree));
        Collections.reverse(polinom);
        Polinom poly = new Polinom();
        poly.initPolinom();
        poly.setPolinom(polinom);
        return poly;
    }

    public int getHighestDegree(){
        polinom.sort(Comparator.comparingInt(Monom::getDegree));
        Collections.reverse(polinom);
        for(Monom m:polinom){
            return m.getDegree();
        }
        return 0;
    }

    @Override
    public String toString() {
        int first = 0;
        StringBuilder poli = new StringBuilder();
        for(Monom m:polinom){
            if(first == 0){

                poli.append(m);
                first = 1;
            }
            else {
                if(m.getCoef()>0) {
                    poli.append("+");
                    poli.append(m);
                }
                else if(m.getCoef()<0){
                    poli.append(m);
                }
            }
        }
        return poli.toString();
    }

    public ArrayList<Monom> getPolinom() {
        return polinom;
    }

    public void setPolinom(ArrayList<Monom> polinom) {
        this.polinom = polinom;
    }

    public Polinom stringToPoly(String string) throws BadPolynomialFormat{
        Polinom polinom = new Polinom();
        polinom.initPolinom();
        Pattern pattern = Pattern.compile("[+-]?\\d+[xX]\\^\\d+|[+-]?\\d+[xX]|[+-]?\\d+|[+-]?[xX]\\^\\d+|[+-]?[xX]");
        Matcher matcher = pattern.matcher(string);
        while(matcher.find()) {
            Monom monom = new Monom();
            String s = matcher.group(0);
            if(!s.matches("[+-]\\d+[xX]\\^\\d+") &&
                    !s.matches("\\d+[xX]\\^\\d+") &&
                    !s.matches("[+-][xX]\\^\\d+") &&
                    !s.matches("[+-][xX]") &&
                    !s.matches("[+-]\\d+") &&
                    !s.matches("[xX]\\^\\d+") &&
                    !s.matches("[+-]?[xX]") &&
                    !s.matches("[+-]?\\d+") &&
                    !s.matches("[+-]?\\d+[xX]") &&
                    !s.matches("[a-v]") &&
                    !s.matches("[y-z]"))
                throw new BadPolynomialFormat(s);

            int degree = 0;
            int coef = 0;
            if (s.contains("x")) {
                if (s.startsWith("x")) {
                    s = "1" + s;
                } else if(s.startsWith("+x")) {
                    s=s.substring(1);
                    s = "1" + s;
                } else if (s.startsWith("-x")) {
                    s=s.substring(1);
                    s = "-1" + s;
                }
                if (s.endsWith("x")) {
                    s = s + "^1";
                }
            } else s = s + "x^0";

            String[] parts = s.split("x\\^");
            String part1 = parts[0];
            String part2 = parts[1];
            degree = Integer.parseInt(part2);
            coef = Integer.parseInt(part1);
            monom.setDegree(degree);
            monom.setCoef(coef);
            polinom.addMonom(monom);
        }
        return polinom;
    }
}
