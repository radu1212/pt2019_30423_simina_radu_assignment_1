package main.java.Logic;

public interface TwoOperandsOperation extends Operation{
    public Result operation(Polinom p1, Polinom p2);
}
