package main.java.UI.Controller;
import main.java.Logic.*;
import main.java.UI.View.*;
import main.java.Exceptions.*;
import javax.swing.*;
import java.util.regex.*;


public class MainController {
    private MainPage mainPage;
    private FirstPage firstPage;
    private SecondPage secondPage;
    public void start(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        mainPage = new MainPage("Polynomials Calculator");
        firstPage = new FirstPage("One Operand Operations");
        secondPage = new SecondPage("Two Operands Operations");

        initializeButtonListeners();

        mainPage.setVisible(true);
        firstPage.setVisible(false);
        secondPage.setVisible(false);
    }

    private void checkSpecialCharacters(String s) throws BadCharacterException{
        if(s.contains("!")||s.contains("@")||s.contains("#")||s.contains("$")||
                s.contains("(")||s.contains("*")||s.contains("&")||s.contains("%")||
                s.contains(")")||s.contains("_")||s.contains("=")||
                s.contains("[")||s.contains("]")||s.contains("{")||s.contains("}")||
                s.contains("\\")||s.contains("|")||s.contains(";")||s.contains(":")||
                s.contains("'")||s.contains("\"")||s.contains(",")||s.contains("<")||
                s.contains(".")||s.contains(">")||s.contains("/")||s.contains("?"))
            throw new BadCharacterException("Bad character!");
    }

    private SimpleResult computations(int operation)throws NullOperandViolation{
        final String poly1 = secondPage.getPoly1();
        final String poly2 = secondPage.getPoly2();
        checkSpecialCharacters(poly1);
        checkSpecialCharacters(poly2);

        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();

        polinom1.initPolinom();
        polinom2.initPolinom();
        polinom1=polinom1.stringToPoly(poly1);
        polinom2=polinom2.stringToPoly(poly2);
        Addition sum = new Addition();
        Substraction substraction = new Substraction();
        Multiplication multiplication = new Multiplication();
        SimpleResult simpleResult = new SimpleResult();
        if(operation == 1)
            simpleResult = sum.operation(polinom1,polinom2);
        else if(operation == 2)
            simpleResult = substraction.operation(polinom1,polinom2);
        else if(operation == 3)
            simpleResult = multiplication.operation(polinom1,polinom2);
        return simpleResult;
    }

    private void initializeButtonListeners(){
        mainPage.addOneOperandOperationButtonActionListener(e->{
            mainPage.setVisible(false);
            firstPage.setVisible(true);
            secondPage.setVisible(false);
        });

        mainPage.addTwoOperandsOperationButtonActionListener(e->{
            mainPage.setVisible(false);
            firstPage.setVisible(false);
            secondPage.setVisible(true);
        });

        firstPage.addBackButtonActionListener(e->{
            mainPage.setVisible(true);
            firstPage.setVisible(false);
            secondPage.setVisible(false);
            firstPage.setPoly("");
            firstPage.setOperationTypo("");
            firstPage.setOutput("");
            firstPage.setLabelResult("");

        });

        firstPage.addDiffButtonActionListener(e->{
            if(!firstPage.getPoly().isEmpty()) {
                firstPage.setOutput("The result of your chosen operation is:");
                firstPage.setOperationTypo("(P1(X))` = ");
                final String poly = firstPage.getPoly();
                checkSpecialCharacters(poly);
                Polinom polinom = new Polinom();
                polinom.initPolinom();
                polinom = polinom.stringToPoly(poly);
                Differentiation differentiation = new Differentiation();
                SimpleResult simpleResult;
                Polinom result = new Polinom();
                result.initPolinom();
                simpleResult = differentiation.operation(polinom);
                result = simpleResult.getPolinom();
                firstPage.setResult(result);
            }
            else throw new NullOperandViolation("NullOperand");
        });

        firstPage.addIntegrateButtonActionListener(e->{
            if(!firstPage.getPoly().isEmpty()) {
                firstPage.setOutput("The result of your chosen operation is:");
                firstPage.setOperationTypo("∫(P1)dX = ");
                final String poly = firstPage.getPoly();
                checkSpecialCharacters(poly);
                Polinom polinom = new Polinom();
                polinom = polinom.stringToPoly(poly);
                Integration integration = new Integration();
                SimpleResult simpleResult;
                Polinom result;
                simpleResult = integration.operation(polinom);
                result = simpleResult.getPolinom();
                firstPage.setResult(result);
            }else throw new NullOperandViolation("NullOperand");
        });

        secondPage.addSumButtonActionListener(e->{
            if(!secondPage.getPoly1().isEmpty() && !secondPage.getPoly2().isEmpty()) {
                secondPage.setOutput("The result of your chosen operation is:");
                secondPage.setType1("P1 + P2 = ");
                secondPage.setType2("");
                secondPage.setType2("");
                SimpleResult simpleResult = computations(1);
                Polinom result = new Polinom();
                result.initPolinom();
                result = simpleResult.getPolinom();
                secondPage.setLabelResult2("");
                secondPage.setResult1(result);
            }else throw new NullOperandViolation("NullOperand");
        });

        secondPage.addBackButtonActionListener(e->{
                mainPage.setVisible(true);
                firstPage.setVisible(false);
                secondPage.setVisible(false);
                secondPage.setLabelResult1("");
                secondPage.setLabelResult2("");
                secondPage.setType1("");
                secondPage.setType2("");
                secondPage.setOutput("");
                secondPage.setPoly("");
        });

        secondPage.addSubButtonActionListener(e->{
            if(!secondPage.getPoly1().isEmpty() && !secondPage.getPoly2().isEmpty()) {
                secondPage.setOutput("The result of your chosen operation is:");
                secondPage.setType1("P1 - P2 = ");
                secondPage.setType2("");
                secondPage.setType2("");
                SimpleResult simpleResult = computations(2);
                Polinom result = new Polinom();
                result.initPolinom();
                result = simpleResult.getPolinom();
                secondPage.setLabelResult2("");
                secondPage.setResult1(result);
            }else throw new NullOperandViolation("NullOperand");

        });

        secondPage.addMultiplyButtonActionListener(e->{
            if(!secondPage.getPoly1().isEmpty() && !secondPage.getPoly2().isEmpty()) {
                secondPage.setOutput("The result of your chosen operation is:");
                secondPage.setType1("P1 * P2 = ");
                secondPage.setType2("");
                secondPage.setType2("");
                SimpleResult simpleResult = computations(3);
                Polinom result = new Polinom();
                result.initPolinom();
                result = simpleResult.getPolinom();
                secondPage.setResult1(result);
                secondPage.setLabelResult2("");
            }else throw new NullOperandViolation("NullOperand");

        });

        secondPage.addDivideButtonActionListener(e->{
            if(!secondPage.getPoly1().isEmpty() && !secondPage.getPoly2().isEmpty()) {
                final String poly1 = secondPage.getPoly1();
                final String poly2 = secondPage.getPoly2();
                if(poly2.equals("0")) throw new ZeroDivisorException("Zero Divisor");
                else {
                    secondPage.setOutput("The result of your chosen operation is:");
                    secondPage.setType1("P1 / P2 = ");
                    secondPage.setType2("P1 % P2 = ");
                    checkSpecialCharacters(poly1);
                    checkSpecialCharacters(poly2);
                    Polinom polinom1 = new Polinom();
                    Polinom polinom2 = new Polinom();
                    Polinom quotient = new Polinom();
                    Polinom remainder = new Polinom();
                    quotient.initPolinom();
                    remainder.initPolinom();
                    polinom1.initPolinom();
                    polinom2.initPolinom();
                    Division division = new Division();
                    polinom1 = polinom1.stringToPoly(poly1);
                    polinom2 = polinom2.stringToPoly(poly2);
                    PairResult pairResult = new PairResult();
                    pairResult = division.operation(polinom1, polinom2);
                    secondPage.setResult1(pairResult.getQuotient());
                    secondPage.setResult2(pairResult.getRemainder());
                }
            }else throw new NullOperandViolation("NullOperand");
        });
    }
}
