package main.java.UI.View;
import main.java.Logic.Polinom;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class FirstPage extends JFrame{

    private JTextField poly;
    private JButton der;
    private JButton integrate;
    private JLabel output;
    private JLabel result;
    private JLabel operationTypo;
    private JButton back;

    public FirstPage(String s) {

        setTitle(s);
        setSize(500,300);

        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setEnabled(true);
        panel.setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        setContentPane(panel);

        JLabel please = new JLabel("Please insert the polynomial:");
        please.setBounds(30,20,430,20);
        please.setHorizontalAlignment(JLabel.CENTER);
        please.setVisible(true);
        panel.add(please);

        poly = new JTextField();
        poly.setBounds(30,40,430,30);
        poly.setHorizontalAlignment(JTextField.CENTER);
        poly.setVisible(true);
        panel.add(poly);

        JLabel choice = new JLabel("Choose the operation you wish to perform:");
        choice.setBounds(30,100,430,20);
        choice.setHorizontalAlignment(JLabel.CENTER);
        choice.setVisible(true);
        panel.add(choice);

        der = new JButton("Differentiate");
        der.setBounds(80,125,140,30);
        der.setHorizontalAlignment(JButton.CENTER);
        der.setVisible(true);
        panel.add(der);

        integrate = new JButton("Integrate");
        integrate.setBounds(280,125,140,30);
        integrate.setHorizontalAlignment(JButton.CENTER);
        integrate.setVisible(true);
        panel.add(integrate);

        output = new JLabel("");
        output.setBounds(30,180,430,20);
        output.setHorizontalAlignment(JLabel.CENTER);
        output.setVisible(true);
        panel.add(output);

        operationTypo = new JLabel("");
        operationTypo.setBounds(10,205,60,30);
        operationTypo.setHorizontalAlignment(JLabel.CENTER);
        operationTypo.setVisible(true);
        panel.add(operationTypo);

        result = new JLabel();
        result.setBounds(70,205,430,30);
        result.setHorizontalAlignment(JTextField.LEFT);
        result.setVisible(true);
        panel.add(result);

        back = new JButton("Menu");
        back.setBounds(410,230,80,30);
        back.setHorizontalAlignment(JButton.CENTER);
        back.setVisible(true);
        panel.add(back);

        //setVisible(true);
    }

    public static void main(String[] args) {
        new FirstPage("One Operand Operation");
    }

    public void addDiffButtonActionListener(final ActionListener actionListener){
        der.addActionListener(actionListener);
    }

    public void addIntegrateButtonActionListener(final ActionListener actionListener){
        integrate.addActionListener(actionListener);
    }

    public void addBackButtonActionListener(final ActionListener actionListener){
        back.addActionListener(actionListener);
    }

    public String getPoly() {
        return poly.getText();
    }

    public void setOutput(String s) {
        this.output.setText(s);
    }

    public void setOperationTypo(String s) {
        this.operationTypo.setText(s);
    }

    public void setResult(Polinom result) {
        this.result.setText(result.toString());
    }

    public void setLabelResult(String s){
        this.result.setText(s);
    }

    public void setPoly(String poly) {
        this.poly.setText(poly);
    }
}
