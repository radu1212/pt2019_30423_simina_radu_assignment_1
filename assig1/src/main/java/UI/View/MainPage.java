package main.java.UI.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MainPage extends JFrame {

    private JLabel info1 ;
    private JLabel info2 ;
    private JButton oneOperand ;
    private JButton twoOperands ;


    public MainPage(String s) throws HeadlessException {

        setTitle(s);

        setSize(500,300);

        JPanel jpanel = new JPanel();

        jpanel.setVisible(true);
        jpanel.setEnabled(true);
        jpanel.setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        setContentPane(jpanel);

        info1 = new JLabel("Please choose wheter you would like to make");
        info1.setBounds(50,70,400,30);
        info1.setHorizontalAlignment(JLabel.CENTER);
        jpanel.add(info1);

        info2 = new JLabel("an operation between two polynomials, or just one:");
        info2.setBounds(50,90,400,30);
        info2.setHorizontalAlignment(JLabel.CENTER);
        jpanel.add(info2);

        oneOperand = new JButton("One Operand Operations");
        oneOperand.setBounds(30,150,210,35);
        oneOperand.setHorizontalAlignment(JButton.CENTER);
        jpanel.add(oneOperand);

        twoOperands = new JButton("Two Operands Operations");
        twoOperands.setBounds(260,150,210,35);
        twoOperands.setHorizontalAlignment(JButton.CENTER);
        jpanel.add(twoOperands);

        setVisible(true);
    }

    public static void main(String[] args) {
        new MainPage("Polynomials Calculator");
    }

    public void addOneOperandOperationButtonActionListener(final ActionListener actionListener){
        oneOperand.addActionListener(actionListener);
    }

    public void addTwoOperandsOperationButtonActionListener(final ActionListener actionListener){
        twoOperands.addActionListener(actionListener);
    }


}
