package main.java.UI.View;


import main.java.Logic.Polinom;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;


public class SecondPage extends JFrame{

    private JTextField poly1;
    private JTextField poly2;
    private JButton sum;
    private JButton diff;
    private JButton mult;
    private JButton div;
    private JLabel result1;
    private JLabel result2;
    private JLabel type1;
    private JLabel type2;
    private JLabel output;
    private JButton back;

    public SecondPage(String s) {

        setTitle(s);
        setSize(700,350);

        JPanel panel = new JPanel();
        panel.setVisible(true);
        panel.setEnabled(true);
        panel.setLayout(null);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        setContentPane(panel);

        JLabel please1 = new JLabel("Please insert the first polynomial:");
        please1.setBounds(30,20,430,20);
        please1.setHorizontalAlignment(JLabel.CENTER);
        please1.setVisible(true);
        panel.add(please1);

        poly1 = new JTextField();
        poly1.setBounds(30,50,430,30);
        poly1.setHorizontalAlignment(JTextField.CENTER);
        poly1.setVisible(true);
        panel.add(poly1);

        JLabel please2 = new JLabel("Please insert the second polynomial:");
        please2.setBounds(30,90,430,20);
        please2.setHorizontalAlignment(JLabel.CENTER);
        please2.setVisible(true);
        panel.add(please2);

        poly2 = new JTextField();
        poly2.setBounds(30,120,430,30);
        poly2.setHorizontalAlignment(JTextField.CENTER);
        poly2.setVisible(true);
        panel.add(poly2);

        JLabel choice = new JLabel("Choose the operation:");
        choice.setBounds(500,20,170,20);
        choice.setHorizontalAlignment(JLabel.CENTER);
        choice.setVisible(true);
        panel.add(choice);


        sum = new JButton("Addition");
        sum.setBounds(500,50,170,30);
        sum.setHorizontalAlignment(JButton.CENTER);
        sum.setVisible(true);
        panel.add(sum);

        diff = new JButton("Subtraction");
        diff.setBounds(500,80,170,30);
        diff.setHorizontalAlignment(JButton.CENTER);
        diff.setVisible(true);
        panel.add(diff);

        mult = new JButton("Multiplication");
        mult.setBounds(500,110,170,30);
        mult.setHorizontalAlignment(JButton.CENTER);
        mult.setVisible(true);
        panel.add(mult);

        div = new JButton("Divide");
        div.setBounds(500,140,170,30);
        div.setHorizontalAlignment(JButton.CENTER);
        div.setVisible(true);
        panel.add(div);

        output = new JLabel("");
        output.setBounds(30,180,640,20);
        output.setHorizontalAlignment(JLabel.CENTER);
        output.setVisible(true);
        panel.add(output);

        result1 = new JLabel();
        result1.setBounds(70,210,550,30);
        result1.setHorizontalAlignment(JTextField.LEFT);
        result1.setVisible(true);
        panel.add(result1);

        result2 = new JLabel();
        result2.setBounds(70,240,550,30);
        result2.setHorizontalAlignment(JTextField.LEFT);
        result2.setVisible(true);
        panel.add(result2);

        type1 = new JLabel();
        type1.setBounds(8,210,62,30);
        type1.setHorizontalAlignment(JTextField.CENTER);
        type1.setVisible(true);
        panel.add(type1);

        type2 = new JLabel();
        type2.setBounds(8,240,62,30);
        type2.setHorizontalAlignment(JTextField.CENTER);
        type2.setVisible(true);
        panel.add(type2);

        back = new JButton("Menu");
        back.setBounds(620,280,70,30);
        back.setHorizontalAlignment(JButton.CENTER);
        back.setVisible(true);
        panel.add(back);
    }

    public static void main(String[] args) {
        new SecondPage("Two Operands Operation");
    }

    public void addSumButtonActionListener(final ActionListener actionListener){
        sum.addActionListener(actionListener);
    }

    public void addSubButtonActionListener(final ActionListener actionListener){
        diff.addActionListener(actionListener);
    }

    public void addMultiplyButtonActionListener(final ActionListener actionListener){
        mult.addActionListener(actionListener);
    }

    public void addDivideButtonActionListener(final ActionListener actionListener){
        div.addActionListener(actionListener);
    }

    public void addBackButtonActionListener(final ActionListener actionListener){
        back.addActionListener(actionListener);
    }

    public String getPoly1() {
        return poly1.getText();
    }

    public String getPoly2() {
        return poly2.getText();
    }

    public void setResult1(Polinom result) {
        this.result1.setText(result.toString());
    }

    public void setResult2(Polinom result2) {
        this.result2.setText(result2.toString());
    }

    public void setType1(String s) {
        this.type1.setText(s);
    }

    public void setType2(String s) {
        this.type2.setText(s);
    }

    public void setOutput(String s){
        this.output.setText(s);
    }

    public void setLabelResult1(String s){ this.result1.setText(s); }

    public void setLabelResult2(String result2) {
        this.result2.setText(result2);
    }

    public void setPoly(String poly) {
        this.poly1.setText(poly);
        this.poly2.setText(poly);
    }
}

