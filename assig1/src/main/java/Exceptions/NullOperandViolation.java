package main.java.Exceptions;
import javax.swing.*;

public class NullOperandViolation extends RuntimeException {
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    public NullOperandViolation(String s) {
        super(s);
        infoBox("An operand field cannot be empty","Empty field");
    }
}
