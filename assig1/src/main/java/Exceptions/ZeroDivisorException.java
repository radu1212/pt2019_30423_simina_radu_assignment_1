package main.java.Exceptions;
import javax.swing.*;

public class ZeroDivisorException extends RuntimeException {
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    public ZeroDivisorException(String s) {
        super(s);
        infoBox("Divisor cannot be zero", "Zero divisor exception");
    }
}
