package main.java.Exceptions;
import javax.swing.*;

public class BadPolynomialFormat extends RuntimeException {
    public BadPolynomialFormat() {
    }
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }
    public BadPolynomialFormat(String s) {
        super(s);
        infoBox("The polynomial you have introduced does not match the pattern\n"+
                "Please make sure that each polynomial you have introduced has only monomials of form:[ (+-)ax^b ]\n"+
                "Please check near: "+s,"Incorrect format");

    }
}
