package main.java.Exceptions;

import javax.swing.*;

public class BadCharacterException extends RuntimeException {
    public static void infoBox(String infoMessage, String titleBar)
    {
        JOptionPane.showMessageDialog(null, infoMessage, titleBar, JOptionPane.INFORMATION_MESSAGE);
    }

    public BadCharacterException(String s) {
        super(s);
        infoBox("Please make sure all the characters you have provided are correct.\n"+
                "Don`t use characters like: !,@,$,%,& and so on...\n Only +,- and ^ are allowed!","Incorrect character");

    }
}
